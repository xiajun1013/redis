package com.cssl.controller;

import com.cssl.pojo.Classes2;
import com.cssl.pojo.Users;
import com.cssl.service.UsersService;
import com.cssl.utils.RedisUtil;
import lombok.val;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
@RestController
public class UsersController {
    @Autowired
    private UsersService usersService;
//    @Autowired
//    private RedisUtil redisUtil;
    @Autowired
    private MongoTemplate mongoTemplate;

//    public List<Users> showUsers(){
//        List<Users> list=null;
//        if (redisUtil.exists("l")){,
//
//        }else {
//
//        }
//        return null;
//    }
    //usersRepository中的模糊查询方法查询
    @RequestMapping("/show")
    public List<Users> showUsers(String name){
        return usersService.findByNamelike(name);
    }

    //调用mongoTemplate模板的方法
    @RequestMapping("/show2")
    public Classes2 showUser(Classes2 classes2){
        classes2 = usersService.findClass();
        return mongoTemplate.save(classes2);
    }

//多对一保存多
    @RequestMapping("/mongodb")
    public Users getUsers(Users users){
        //System.out.println("get:"+id);
        //条件构造器 就是相当于sql语句where后的条件 当前代码意思就是cid等于users中的classes2中的cid
        //值  where cid=#{users.classes.cid}
        Query query = new Query(Criteria.where("cid").is(users.getClasses2().getCid()));

        //Users one = mongoTemplate.findOne(query, Users.class);
        //mongoTemplate模板 可用于增删查改  这里的意思是根据query里的条件查询 classes2.class
        //的意思应该是查询结果映射到classes2对象中
        Classes2 classes2 = mongoTemplate.findOne(query, Classes2.class);
        Set<Users> users1 = classes2.getUsers();
        if (users1==null){
            users1=new HashSet<>();
        }
        users1.add(users);
        Update update =new Update();
        update.set("users",users1);
        mongoTemplate.findAndModify(query,update,Classes2.class);
        return mongoTemplate.save(users);//id相同则修改
    }
}