package com.cssl.dao;

import com.cssl.pojo.Classes2;
import com.cssl.pojo.Users;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersDao {
    public int adduser(Users users);

    public Users showByid(int id);

    public List<Users> showUser();

    public int del(int id);

    public Classes2 selectClasses(int cid);
}