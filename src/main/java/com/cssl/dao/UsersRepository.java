package com.cssl.dao;

import com.cssl.pojo.Users;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UsersRepository extends MongoRepository<Users,Integer> {
    public List<Users> findByName(String name);

    public List<Users> findByNameLike(String name);
}
