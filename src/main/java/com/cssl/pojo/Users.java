package com.cssl.pojo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
@Document("usersxj")
@Data//set get等方法的注解 idea前提得下载插件
public class Users implements Serializable {
    private Integer uid;
    private String name;
    private String password;
    private String telephone;
    private String username;
    private Classes2 classes2;

}
