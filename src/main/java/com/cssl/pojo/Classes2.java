package com.cssl.pojo;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Set;
@Data
public class Classes2 {

    @Id
    private Integer cid;
    private String cname;
    private Set<Users> users;
}
