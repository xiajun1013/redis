package com.cssl.service;

import com.cssl.pojo.Classes2;
import com.cssl.pojo.Users;
import org.junit.internal.Classes;

import java.util.List;

public interface UsersService {
//    public int adduser(Users users);
//
//    public Users showByid(int id);
//
//    public List<Users> showUser();
//
//    public int del(int id);



    public List<Users> findAll();

    public Users saveByMongo(Users user);

    public List<Users> findByNamelike(String name);

    public int remove(int id);

    public Classes2 findClass();

}
