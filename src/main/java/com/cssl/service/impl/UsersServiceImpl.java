package com.cssl.service.impl;

import com.cssl.dao.UsersDao;
import com.cssl.dao.UsersRepository;
import com.cssl.pojo.Classes2;
import com.cssl.pojo.Users;
import com.cssl.service.UsersService;


import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {
    @Resource
    private UsersDao usersDao;
    @Resource
    private UsersRepository usersRepository;


    @Override
    public List<Users> findAll() {
        return null;
    }

    @Override
    public Users saveByMongo(Users user) {
        return null;
    }

    @Override
    public List<Users> findByNamelike(String name) {
        return usersRepository.findByNameLike(name);
    }

    @Override
    public int remove(int id) {
        try {
            //调用的时父类的方法
            usersRepository.deleteById(id);
        }catch (Exception e){
            return 0;
        }
        return 1;
    }

    @Override
    public Classes2 findClass() {
        return usersDao.selectClasses(2);
    }
}
